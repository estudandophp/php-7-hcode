<?php

  $a = 55.0;

  $b = 55;

  // A maior que B?
  var_dump($a > $b);

  echo '<br>';

  // A menor que B?
  var_dump($a < $b);

  echo '<br>';

  // A igual a B?
  var_dump($a == $b);

  echo '<br>';

  // A idêntico a B?
  var_dump($a === $b);

  echo '<br>';

  var_dump($a != $b);

  echo '<br>';

  var_dump($a !== $b);
  ?>
