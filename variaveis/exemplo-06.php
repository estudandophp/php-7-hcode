<?php

  // Declaração de variável global
  $nome = 'Leonardo';

  function teste(){
    // nome é uma variável global, mas mesmo assim, precisa permitir seu uso na função
    global $nome;
    echo $nome . '<br><br>';

  }

  function teste2(){
    // Pode-se declarar outra variável dentro de uma função, com o mesmo nome de uma global,
    // ela não existirá fora da função.
    $nome = 'Outro nome';
    echo $nome;

  }

  teste();
  teste2();

?>
