<?php

    // Declara a variável $nome, com o meu nome dentro dela.
    $nome = "Leonardo Lima";

    // Imprime o conteúdo da variável $nome na página, pula para a próxima linha, seguido de mais uma quebra de linha.
    echo $nome . "<br><br>";
    
    // Mostra o tipo da variável $nome, seu tamanho, e seu conteúdo.
    var_dump($nome);

?>
