<?php

  // Pega a variável 'nome' pela URL
  $nome = $_GET["nome"];

  var_dump($nome);
  echo "<br><br>";

  // Pega um valor pela URL e força sua conversão para inteiro
  $idade = (int) $_GET["idade"];

  var_dump($idade);
  echo "<br><br>";

  // Pega o IP do usuário
  $ip = $_SERVER["REMOTE_ADDR"];

  var_dump($ip);
  echo "<br><br>";

  // Pega o caminho da página atual e a página em si logo após
  $pagina_atual = $_SERVER["SCRIPT_NAME"];

  var_dump($pagina_atual);
  echo "<br><br>";

 ?>
