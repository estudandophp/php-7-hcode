<?php

// Variáveis simples

// String com aspas duplas e aspas simples.
$nome = "Leonardo";
$site = 'www.hcode.com.br';

// Tipos Inteiro, Float, e Boolean
$ano = 1999;
$salario = 5500.99;
$bloqueado = false;

/////////////////////////////////////////////////

// Variáveis compostas.

// Tipo array (lista).
//     posições     0          1         2
$frutas = array("abacaxi", "laranja", "manga");

// Imprime o conteúdo da posição 2, manga
echo $frutas[2];

// Pula 2 linhas.
echo "<br><br>";

// Tipo Objeto.

$nascimento = new DateTime();

var_dump($nascimento);

/////////////////////////////////////////////////

// Tipo Stream.
$arquivo = fopen("exemplo-03.php", "r");

var_dump($arquivo);

// Tipo nulo.
$nulo = NULL;

// Nulo é diferente de vazio
$vazio = "";

?>