<?php

// Declara a variável $nome.
$nome = "Leonardo";

// Declara a variável $sobrenome;
$sobrenome = "Lima";

// Declara a variável $nomeCompleto. Concatenando as variáveis $nome e $sobrenome.
$nomeCompleto = $nome . " " . $sobrenome; /*    Para concatenar no PHP, usa-se '.'(ponto).
Adicionado um espaço no meio para separar as palavras   */

// Exibe a variável $nomeCompleto.
echo $nomeCompleto;

echo "<br><br>Tem mais coisas pra ver no código fonte";

/*  Para a execução do código aqui. (Pra testar outras coisas sem ficar fazendo um novo arquivo.
    Pode remover essa linha sem problemas.)  */
exit;

// Declara a variável $anoNascimento.
$anoNascimento = 1999;

echo $anoNascimento;

// Pula uma linha.
echo "<br>";

// Método unset() usado para DELETAR uma variável.
unset($anoNascimento);

// Verifica se a variável existe/foi declarada. Método isset().
// Só será impresso seu conteúdo se ela existir.
if (isset($anoNascimento)){

    echo $anoNascimento;

} else {

    echo "Variável não existe, ou foi apagada!";

}

?>
